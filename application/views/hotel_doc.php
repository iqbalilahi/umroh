<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Hotel List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Kamar</th>
		<th>Nama Hotel</th>
		<th>Lokasi Hotel</th>
		<th>Desc Hotel</th>
		
            </tr><?php
            foreach ($hotel_data as $hotel)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $hotel->id_kamar ?></td>
		      <td><?php echo $hotel->nama_hotel ?></td>
		      <td><?php echo $hotel->lokasi_hotel ?></td>
		      <td><?php echo $hotel->desc_hotel ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
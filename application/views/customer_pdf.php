<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Customer List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Username</th>
		<th>Nama Lengkap </th>
		<th>Nama Ibu</th>
		<th>Tempat</th>
		<th>Tanggal Lahir</th>
		<th>Jenis Kelamin</th>
		<th>Penyakit Khusus</th>
		<th>No Telp</th>
		<th>Alamat</th>
		<th>Password</th>
		
            </tr><?php
            foreach ($customer_data as $customer)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $customer->username ?></td>
		      <td><?php echo $customer->nama_lengkap_c ?></td>
		      <td><?php echo $customer->nama_ibu ?></td>
		      <td><?php echo $customer->tempat ?></td>
		      <td><?php echo $customer->tanggal_lahir ?></td>
		      <td><?php echo $customer->jenis_kelamin ?></td>
		      <td><?php echo $customer->umur ?></td>
		      <td><?php echo $customer->penyakit_khusus ?></td>
		      <td><?php echo $customer->no_telp ?></td>
		      <td><?php echo $customer->alamat ?></td>
		      <td><?php echo $this->encrypt->encode($customer->password) ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
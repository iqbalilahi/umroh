
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                  <h3 class='box-title'>PAKET LIST <?php echo anchor('paket/create/','Create',array('class'=>'btn btn-danger btn-sm'));?>
		<?php echo anchor(site_url('paket/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
		<?php echo anchor(site_url('paket/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
		<?php 
        //echo anchor(site_url('paket/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>
                </div><!-- /.box-header -->
                <div class='box-body'>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Nama Paket</th>
		    <th>Nama Hotel</th>
		    <th>Nama Transport</th>
		    <th>Nama Pendamping</th>
		    <th>Tahun</th>
		    <th>Harga Paket</th>
		    <th>Desc Paket</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($paket_data as $paket)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $paket->nama_paket ?></td>
		    <td><?php echo $paket->nama_hotel; echo "<br>  : " ; echo $paket->desc_hotel ?></td>
		    <td><?php echo $paket->nama_transport ?></td>
		    <td><?php echo $paket->nama_lengkap_p ?></td>
		    <td><?php echo $paket->tahun; echo "<br> - kloter: " ; echo $paket->kloter ?></td>
		    <td><?php echo $paket->harga_paket ?></td>
		    <td><?php echo $paket->desc_paket ?></td>
		    <td style="text-align:center" width="140px">
			<?php 
			echo anchor(site_url('paket/read/'.$paket->id_paket),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-danger btn-sm')); 
			echo '  '; 
			echo anchor(site_url('paket/update/'.$paket->id_paket),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-danger btn-sm')); 
			echo '  '; 
			echo anchor(site_url('paket/delete/'.$paket->id_paket),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
                    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
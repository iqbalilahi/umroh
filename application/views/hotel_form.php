<!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                
                  <h3 class='box-title'>HOTEL</h3>
                      <div class='box box-primary'>
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Kamar <?php echo form_error('id_kamar') ?></td>
            <!-- <td><input type="text" class="form-control" name="id_kamar" id="id_kamar" placeholder="Id Kamar" value="<?php echo $id_kamar; ?>" />
        </td> -->
        <td>
            <div class="form-group">
                        <label>Jenis Kamar</label>
                        <select class="form-control" name="id_kamar" id="id_kamar">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($id_kamar as $kot) {
                                ?>
                                <!--di sini kita tambahkan class berisi id provinsi-->
                                <option <?php echo $kamar_selected == $kot->id_kamar ? 'selected="selected"' : '' ?>
                                    class="<?php echo $kot->id_kamar ?>" value="<?php echo $kot->id_kamar ?>"><?php echo $kot->jenis_kamar ; echo " - Kasur : " ; echo $kot->banyak_kasur ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
        </td>
	    <tr><td>Nama Hotel <?php echo form_error('nama_hotel') ?></td>
            <td><input type="text" class="form-control" name="nama_hotel" id="nama_hotel" placeholder="Nama Hotel" value="<?php echo $nama_hotel; ?>" />
        </td>
	    <tr><td>Lokasi Hotel <?php echo form_error('lokasi_hotel') ?></td>
            <td><input type="text" class="form-control" name="lokasi_hotel" id="lokasi_hotel" placeholder="Lokasi Hotel" value="<?php echo $lokasi_hotel; ?>" />
        </td>
	    <tr><td>Desc Hotel <?php echo form_error('desc_hotel') ?></td>
            <td><input type="text" class="form-control" name="desc_hotel" id="desc_hotel" placeholder="Desc Hotel" value="<?php echo $desc_hotel; ?>" />
        </td>
	    <input type="hidden" name="id_hotel" value="<?php echo $id_hotel; ?>" /> 
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('hotel') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table></form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Pendaftaran List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Customer</th>
		<th>Id Paket</th>
		<th>Tgl Daftar</th>
		<th>Tgl Bayar</th>
		<th>Status Pesanan</th>
		<th>Status Bayar</th>
		<th>Status Verifikasi Berkas</th>
		
            </tr><?php
            foreach ($pendaftaran_data as $pendaftaran)
            {
                $pesan = $pendaftaran->status_pesanan==1?'Terverifikasi':'Belum Terverifikasi';
                $bayar = $pendaftaran->status_bayar==1?'Sudah Bayar':'Belum Bayar';
                $berkas = $pendaftaran->status_verifikasi_berkas==1?'Sudah Mengirim Berkas':'Belum Mengirim Berkas';
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $pendaftaran->nama_lengkap_c ?></td>
		      <td><?php echo $pendaftaran->nama_paket ?></td>
		      <td><?php echo $pendaftaran->tgl_daftar ?></td>
		      <td><?php echo $pendaftaran->tgl_bayar ?></td>
		      <td><?php echo $pesan ?></td>
            <td><?php echo $bayar ?></td>
            <td><?php echo $berkas ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Persyaratan List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Customer</th>
		<th>Paspor</th>
		<th>Surat Nikah</th>
		<th>Ktp</th>
		<th>Kartu Keluarga</th>
		<th>Kartu Kuning</th>
		
            </tr><?php
            foreach ($persyaratan_data as $persyaratan)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $persyaratan->id_customer ?></td>
		      <td><?php echo $persyaratan->paspor ?></td>
		      <td><?php echo $persyaratan->surat_nikah ?></td>
		      <td><?php echo $persyaratan->ktp ?></td>
		      <td><?php echo $persyaratan->kartu_keluarga ?></td>
		      <td><?php echo $persyaratan->kartu_kuning ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
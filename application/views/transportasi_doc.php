<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Transportasi List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama Transport</th>
		<th>Code Transport</th>
		<th>Desc Transport</th>
		<th>Seat Qty</th>
		
            </tr><?php
            foreach ($transportasi_data as $transportasi)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $transportasi->nama_transport ?></td>
		      <td><?php echo $transportasi->code_transport ?></td>
		      <td><?php echo $transportasi->desc_transport ?></td>
		      <td><?php echo $transportasi->seat_qty ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
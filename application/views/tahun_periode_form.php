<!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                
                  <h3 class='box-title'>TAHUN_PERIODE</h3>
                      <div class='box box-primary'>
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Tahun <?php echo form_error('tahun') ?></td>
            <td><input type="text" class="form-control" name="tahun" id="tahun" placeholder="Tahun" value="<?php echo $tahun; ?>" />
        </td>
	    <tr><td>Kloter <?php echo form_error('kloter') ?></td>
            <td><input type="text" class="form-control" name="kloter" id="kloter" placeholder="Kloter" value="<?php echo $kloter; ?>" />
        </td>
	    <tr><td>Tgl Berangkat <?php echo form_error('tgl_berangkat') ?></td>
            <td><input type="text" class="form-control" name="tgl_berangkat" id="tgl_berangkat" placeholder="Tgl Berangkat" value="<?php echo $tgl_berangkat; ?>" />
        </td>
	    <input type="hidden" name="id_tahun" value="<?php echo $id_tahun; ?>" /> 
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('tahun_periode') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table></form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Pendamping List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama Lengkap</th>
		<th>Tempat</th>
		<th>Tanggal Ahir</th>
		<th>Jenis Kelamin</th>
		<th>No Telp</th>
		<th>Alamat</th>
		
            </tr><?php
            foreach ($pendamping_data as $pendamping)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $pendamping->nama_lengkap_p ?></td>
		      <td><?php echo $pendamping->tempat ?></td>
		      <td><?php echo $pendamping->tanggal_ahir ?></td>
		      <td><?php echo $pendamping->jenis_kelamin ?></td>
		      <td><?php echo $pendamping->no_telp ?></td>
		      <td><?php echo $pendamping->alamat ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>

        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                  <h3 class='box-title'>CUSTOMER LIST <?php echo anchor('customer/create/','Create',array('class'=>'btn btn-danger btn-sm'));?>
		<?php echo anchor(site_url('customer/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
		<?php echo anchor(site_url('customer/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
		<?php 
        //echo anchor(site_url('customer/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>
                </div><!-- /.box-header -->
                <div class='box-body'>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <!-- <th>Username</th> -->
		    <th>Nama Lengkap</th>
		    <th>Nama Ibu</th>
		    <th>Tempat</th>
		    <th>Tanggal Lahir</th>
		    <th>Jenis Kelamin</th>
		    <th>Penyakit Khusus</th>
		    <th>No Telp</th>
		    <th>Alamat</th>
		    <th style="display: none">Password</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($customer_data as $customer)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <!-- <td><?php echo $customer->username ?></td> -->
		    <td><?php echo $customer->nama_lengkap_c ?></td>
		    <td><?php echo $customer->nama_ibu ?></td>
		    <td><?php echo $customer->tempat ?></td>
		    <td><?php echo $customer->tanggal_lahir ?></td>
		    <td><?php echo $customer->jenis_kelamin ?></td>
		    <td><?php echo $customer->penyakit_khusus ?></td>
		    <td><?php echo $customer->no_telp ?></td>
		    <td><?php echo $customer->alamat ?></td>
		    <td style="display: none"><?php echo $customer->password ?></td>
		    <td style="text-align:center" width="140px">
			<?php 
			echo anchor(site_url('customer/read/'.$customer->id_customer),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-danger btn-sm')); 
			echo '  '; 
			echo anchor(site_url('customer/update/'.$customer->id_customer),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-danger btn-sm')); 
			echo '  '; 
			echo anchor(site_url('customer/delete/'.$customer->id_customer),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
                    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
<!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                
                  <h3 class='box-title'>PAKET</h3>
                      <div class='box box-primary'>
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Nama Paket <?php echo form_error('nama_paket') ?></td>
            <td><input type="text" class="form-control" name="nama_paket" id="nama_paket" placeholder="Nama Paket" value="<?php echo $nama_paket; ?>" />
        </td>
	    <tr><td>Id Hotel <?php echo form_error('id_hotel') ?></td>
            <td>
            <div class="form-group">
                        <label>Hotel</label>
                        <select class="form-control" name="id_hotel" id="id_hotel">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($id_hotel as $kot) {
                                ?>
                                <!--di sini kita tambahkan class berisi id provinsi-->
                                <option <?php echo $hotel_selected == $kot->id_hotel ? 'selected="selected"' : '' ?>
                                    class="<?php echo $kot->id_hotel ?>" value="<?php echo $kot->id_hotel ?>"><?php echo $kot->nama_hotel; echo " - "; echo $kot->desc_hotel ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
        </td>
	    <tr><td>Id Transport <?php echo form_error('id_transport') ?></td>
            <td>
            <div class="form-group">
                        <label>Nama Pesawat</label>
                        <select class="form-control" name="id_transport" id="id_transport">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($id_transport as $kot) {
                                ?>
                                <!--di sini kita tambahkan class berisi id provinsi-->
                                <option <?php echo $transport_selected == $kot->id_transport ? 'selected="selected"' : '' ?>
                                    class="<?php echo $kot->id_transport ?>" value="<?php echo $kot->id_transport ?>"><?php echo $kot->nama_transport; echo " - "; echo $kot->code_transport ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
        </td>
	    <tr><td>Id Pendamping <?php echo form_error('id_pendamping') ?></td>
            <td>
            <div class="form-group">
                        <label>Nama Pendamping</label>
                        <select class="form-control" name="id_pendamping" id="id_pendamping">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($id_pendamping as $kot) {
                                ?>
                                <!--di sini kita tambahkan class berisi id provinsi-->
                                <option <?php echo $pendamping_selected == $kot->id_pendamping ? 'selected="selected"' : '' ?>
                                    class="<?php echo $kot->id_pendamping ?>" value="<?php echo $kot->id_pendamping ?>"><?php echo $kot->nama_lengkap_p ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
        </td>
	    <tr><td>Id Tahun <?php echo form_error('id_tahun') ?></td>
            <td>
            <div class="form-group">
                        <label>Kloter</label>
                        <select class="form-control" name="id_tahun" id="id_tahun">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($id_tahun as $kot) {
                                ?>
                                <!--di sini kita tambahkan class berisi id provinsi-->
                                <option <?php echo $tahun_selected == $kot->id_tahun ? 'selected="selected"' : '' ?>
                                    class="<?php echo $kot->id_tahun ?>" value="<?php echo $kot->id_tahun ?>"><?php echo "Tahun - "; echo $kot->tahun; echo ", Kloter - "; echo $kot->kloter ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
        </td>
	    <tr><td>Harga Paket <?php echo form_error('harga_paket') ?></td>
            <td><input type="text" class="form-control" name="harga_paket" id="harga_paket" placeholder="Harga Paket" value="<?php echo $harga_paket; ?>" />
        </td>
	    <tr><td>Desc Paket <?php echo form_error('desc_paket') ?></td>
            <td><input type="text" class="form-control" name="desc_paket" id="desc_paket" placeholder="Desc Paket" value="<?php echo $desc_paket; ?>" />
        </td>
	    <input type="hidden" name="id_paket" value="<?php echo $id_paket; ?>" /> 
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('paket') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table></form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
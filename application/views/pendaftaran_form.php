<!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                
                  <h3 class='box-title'>PENDAFTARAN</h3>
                      <div class='box box-primary'>
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Id Customer <?php echo form_error('id_customer') ?></td>
            <td>
            <div class="form-group">
                        <label>Customer</label>
                        <select class="form-control" name="id_customer" id="id_customer">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($id_customer as $kot) {
                                ?>
                                <!--di sini kita tambahkan class berisi id provinsi-->
                                <option <?php echo $customer_selected == $kot->id_customer ? 'selected="selected"' : '' ?>
                                    class="<?php echo $kot->id_customer ?>" value="<?php echo $kot->id_customer ?>"><?php echo $kot->nama_lengkap_c ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
        </td>
	    <tr><td>Id Paket <?php echo form_error('id_paket') ?></td>
            <td>
            <div class="form-group">
                        <label>Paket</label>
                        <select class="form-control" name="id_paket" id="id_paket">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($id_paket as $kot) {
                                ?>
                                <!--di sini kita tambahkan class berisi id provinsi-->
                                <option <?php echo $paket_selected == $kot->id_paket ? 'selected="selected"' : '' ?>
                                    class="<?php echo $kot->id_paket ?>" value="<?php echo $kot->id_paket ?>"><?php echo $kot->nama_paket ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
        </td>
	    <tr><td>Tgl Daftar <?php echo form_error('tgl_daftar') ?></td>
            <td><input type="date" class="form-control" name="tgl_daftar" id="tgl_daftar" placeholder="Tgl Daftar" value="<?php echo $tgl_daftar; ?>" />
        </td>
	    <tr><td>Tgl Bayar <?php echo form_error('tgl_bayar') ?></td>
            <td><input type="hidden" class="form-control" name="tgl_bayar" id="tgl_bayar" placeholder="Tgl Bayar" value="0000-00-00" />
        </td>
	    <tr><td>Status Pesanan <?php echo form_error('status_pesanan') ?></td>
            <td><input type="hidden" class="form-control" name="status_pesanan" id="status_pesanan" placeholder="Status pesanan" value="0" />
        </td>
	    <tr><td>Status Bayar <?php echo form_error('status_bayar') ?></td>
            <td><input type="hidden" class="form-control" name="status_bayar" id="status_bayar" placeholder="Status Bayar" value="0" />
        </td>
	    <tr><td>Status Verifikasi Berkas <?php echo form_error('status_verifikasi_berkas') ?></td>
            <td><input type="hidden" class="form-control" name="status_verifikasi_berkas" id="status_verifikasi_berkas" placeholder="Status Verifikasi Berkas" value="0" />
        </td>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('verifikasi') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table></form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
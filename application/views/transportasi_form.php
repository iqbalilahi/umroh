<!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                
                  <h3 class='box-title'>TRANSPORTASI</h3>
                      <div class='box box-primary'>
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Nama Transport <?php echo form_error('nama_transport') ?></td>
            <td><input type="text" class="form-control" name="nama_transport" id="nama_transport" placeholder="Nama Transport" value="<?php echo $nama_transport; ?>" />
        </td>
	    <tr><td>Code Transport <?php echo form_error('code_transport') ?></td>
            <td><input type="text" class="form-control" name="code_transport" id="code_transport" placeholder="Code Transport" value="<?php echo $code_transport; ?>" />
        </td>
	    <tr><td>Desc Transport <?php echo form_error('desc_transport') ?></td>
            <td><input type="text" class="form-control" name="desc_transport" id="desc_transport" placeholder="Desc Transport" value="<?php echo $desc_transport; ?>" />
        </td>
	    <tr><td>Seat Qty <?php echo form_error('seat_qty') ?></td>
            <td><input type="text" class="form-control" name="seat_qty" id="seat_qty" placeholder="Seat Qty" value="<?php echo $seat_qty; ?>" />
        </td>
	    <input type="hidden" name="id_transport" value="<?php echo $id_transport; ?>" /> 
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('transportasi') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table></form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
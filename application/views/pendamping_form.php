<!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                
                  <h3 class='box-title'>PENDAMPING</h3>
                      <div class='box box-primary'>
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Nama Lengkap <?php echo form_error('nama_lengkap_p') ?></td>
            <td><input type="text" class="form-control" name="nama_lengkap_p" id="nama_lengkap_p" placeholder="Nama Lengkap" value="<?php echo $nama_lengkap_p; ?>" />
        </td>
	    <tr><td>Tempat <?php echo form_error('tempat') ?></td>
            <td><input type="text" class="form-control" name="tempat" id="tempat" placeholder="Tempat" value="<?php echo $tempat; ?>" />
        </td>
	    <tr><td>Tanggal Ahir <?php echo form_error('tanggal_ahir') ?></td>
            <td><input type="date" class="form-control" name="tanggal_ahir" id="tanggal_ahir" placeholder="Tanggal Ahir" value="<?php echo $tanggal_ahir; ?>" />
        </td>
	    <tr><td>Jenis Kelamin <?php echo form_error('jenis_kelamin') ?></td>
            <td>
              <div class="form-group">
                    <select class="form-control select2 select2-hidden-accessible" name="jenis_kelamin" id="jenis_kelamin">
                        <option value="Laki-laki">Laki - Laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
            </div>
        </td>
	    <tr><td>No Telp <?php echo form_error('no_telp') ?></td>
            <td><input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="No Telp" required value="<?php echo $no_telp; ?>" />
        </td>
	    <tr><td>Alamat <?php echo form_error('alamat') ?></td>
            <td><input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?php echo $alamat; ?>" />
        </td>
	    <input type="hidden" name="id_pendamping" value="<?php echo $id_pendamping; ?>" /> 
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('pendamping') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table></form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
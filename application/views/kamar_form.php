<!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                
                  <h3 class='box-title'>KAMAR</h3>
                      <div class='box box-primary'>
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Jenis Kamar <?php echo form_error('jenis_kamar') ?></td>
            <td><input type="text" class="form-control" name="jenis_kamar" id="jenis_kamar" placeholder="Jenis Kamar" value="<?php echo $jenis_kamar; ?>" />
        </td>
	    <tr><td>Nama Kamar <?php echo form_error('nama_kamar') ?></td>
            <td><input type="text" class="form-control" name="nama_kamar" id="nama_kamar" placeholder="Nama Kamar" value="<?php echo $nama_kamar; ?>" />
        </td>
	    <tr><td>Banyak Kasur <?php echo form_error('banyak_kasur') ?></td>
            <td><input type="text" class="form-control" name="banyak_kasur" id="banyak_kasur" placeholder="Banyak Kasur" value="<?php echo $banyak_kasur; ?>" />
        </td>
	    <input type="hidden" name="id_kamar" value="<?php echo $id_kamar; ?>" /> 
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('kamar') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table></form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
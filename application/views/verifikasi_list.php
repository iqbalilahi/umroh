
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                  <h3 class='box-title'>VERIFIKASI LIST</h3>
                </div><!-- /.box-header -->
                <div class='box-body'>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Nama Customer</th>
		    <th>Nama Paket</th>
		    <th>Tgl Daftar</th>
		    <th>Tgl Bayar</th>
		    <th>Status Pesanan</th>
		    <th>Status Bayar</th>
		    <th>Status Verifikasi Berkas</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($pendaftaran_data as $pendaftaran)
            {
                 /*$pesan = $pendaftaran->status_pesanan==1?'Sudah Terverifikasi':anchor(site_url('verifikasi/pesanan/'.$pendaftaran->id),'Belum Terverifikasi','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
               /* $bayar = $pendaftaran->status_bayar==1?'Sudah  Bayar':anchor(site_url('verifikasi/bayaran/'.$pendaftaran->id),'Belum Bayar','class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
               $berkas = $berkas = $pendaftaran->status_verifikasi_berkas==1?'Sudah Mengirim Berkas':'<a data-toggle="modal" data-target="#modal_verifikasi" class="btn btn-danger">Belum Mengirim Berkas</a>';
                */?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $pendaftaran->nama_lengkap_c ?></td>
		    <td><?php echo $pendaftaran->nama_paket ; echo "<br> Harga Paket : " ; echo $pendaftaran->harga_paket ?></td>
		    <td><?php echo $pendaftaran->tgl_daftar ?></td>
		    <td><?php echo $pendaftaran->tgl_bayar ?></td>
		    <td><a data-toggle="modal" data-target="#modal_pesanan_<?php echo $pendaftaran->id; ?>" class="<?php if($pendaftaran->status_pesanan  == 0 ) {echo "btn btn-danger";}
                        else  {echo "btn btn-success disabled";} ?>">
                <?php if($pendaftaran->status_pesanan  == 0 ) {echo "Belum Verifikasi Pesan";}
                        else  {echo "Sudah Verifikasi Pesan";} ?></a></td>
		    <td><a data-toggle="modal" data-target="#modal_bayar_<?php echo $pendaftaran->id; ?>" class="<?php if($pendaftaran->status_bayar  == 0 ) {echo "btn btn-danger";}
                        else  {echo "btn btn-success disabled";} ?>">
                <?php if($pendaftaran->status_bayar  == 0 ) {echo "Belum Bayar";}
                        else  {echo "Sudah Bayar";} ?></a></td>
		    <td><a data-toggle="modal" data-target="#modal_verifikasi_<?php echo $pendaftaran->id; ?>" class="<?php if($pendaftaran->status_verifikasi_berkas  == 0 ) {echo "btn btn-danger";}
                        else  {echo "btn btn-success disabled";} ?>">
                <?php if($pendaftaran->status_verifikasi_berkas  == 0 ) {echo "Belum Mengirim Berkas";}
                        else  {echo "Sudah Mengirim Berkas";} ?></a></td>
		    <td style="text-align:center" width="140px">
			<?php 
			// echo anchor(site_url('pendaftaran/read/'.$pendaftaran->id),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-danger btn-sm')); 
			// echo '  '; 
			echo anchor(site_url('pendaftaran/update/'.$pendaftaran->id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-danger btn-sm')); 
			echo '  '; 
			echo anchor(site_url('pendaftaran/delete/'.$pendaftaran->id),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
			?>
		    </td>
	        </tr>
            <div class="modal fade" id="modal_verifikasi_<?php echo $pendaftaran->id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2>Verifikasi Berkas</h2>
                    </div>
                    <div class="modal-body">
                        
                            <form action="<?php echo base_url().'verifikasi/syarat/'.$pendaftaran->id?>" method="post" enctype="multipart/form-data" role="form">
                                    <div class="form-group">
                                        
                                            Foto Paspor
                                        
                                        <div class="col-ms-10">
                                            <input type="file" id="id-input-file-2" name="userfile[]" multiple="multiple"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        
                                            Foto Surat Nikah
                                        
                                        <div class="col-ms-10">
                                            <input type="file" id="id-input-file-2" name="userfile[]" multiple="multiple"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        
                                            Foto KTP
                                        
                                        <div class="col-ms-10">
                                            <input type="file" id="id-input-file-2" name="userfile[]" multiple="multiple"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        
                                            Foto KK
                                        
                                        <div class="col-ms-10">
                                            <input type="file" id="id-input-file-2" name="userfile[]" multiple="multiple"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        
                                            Foto Kartu Kuning
                                        
                                        <div class="col-ms-10">
                                            <input type="file" id="id-input-file-2" name="userfile[]" multiple="multiple"/>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <!-- <input type="file" name="userfile[]" multiple="multiple"></td> -->
                                    <label class="col-md-2 col-form-label">Pilih Status</label>
                                    <select class="form-control select2 select2-hidden-accessible" style="width: 40%;" name="status_verifikasi_berkas" id="status_verifikasi_berkas">
                                        <option value="0">Belum Verifikasi Berkas</option>
                                        <option value="1">Sudah Verifikasi Berkas</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                 <label class="col-md-2 col-form-label">Nama Customer</label>
                                 <div class="col-md-10">
                                    <input type="text" name="nama_lengkap_c"   value="<?php echo $pendaftaran->nama_lengkap_c; ?>" readonly>
                                    <input type="hidden" name="id_customer"   value="<?php echo $pendaftaran->id_customer; ?>" readonly>
                                 </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="submit" name="submit" class="btn btn-primary">Verifikasi </button> 
                                </div>
                            </form>
                        
                        <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal_bayar_<?php echo $pendaftaran->id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="container">
                            <h2>Pembayaran</h2>
                            <form action="<?php echo base_url().'verifikasi/bayaran/'.$pendaftaran->id?>" method="post">
                               
                                <div class="form-group">
                                    <label class="col-md-2 col-form-label">Pilih Status</label>
                                    <select class="form-control select2 select2-hidden-accessible" style="width: 20%;" name="status_bayar" id="status_bayar">
                                        <option value="0">Belum Bayar</option>
                                        <option value="1">Sudah Bayar</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                 <label class="col-md-2 col-form-label">Nama Customer</label>
                                     <div class="col-md-10">
                                    <input type="text" name="nama_lengkap_c"   value="<?php echo $pendaftaran->nama_lengkap_c; ?>" readonly>
                                 </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Bayar</button> 
                            </form>
                        <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal_pesanan_<?php echo $pendaftaran->id; ?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="container">
                            <h2>Verifikasi Pemesanan</h2>
                            <form action="<?php echo base_url().'verifikasi/pesanan/'.$pendaftaran->id?>" method="post">
                                <div class="form-group">
                                    <label class="col-md-2 col-form-label">Pilih Status</label>
                                    <select class="form-control select2 select2-hidden-accessible" style="width: 20%;" name="status_pesanan" id="status_pesanan">
                                        <option value="0">Belum Verifikasi Pesan</option>
                                        <option value="1">Sudah Verifikasi Pesan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                 <label class="col-md-2 col-form-label">Nama Customer</label>
                                     <div class="col-md-10">
                                    <input type="text" name="nama_lengkap_c"   value="<?php echo $pendaftaran->nama_lengkap_c; ?>" readonly>
                                 </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Verifikasi Pemesanan</button> 
                            </form>
                        <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });

            function getDate()
            {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();
                if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm}
                today = yyyy+""+mm+""+dd;

                document.getElementById("tgl_bayar").value = today;
            }

            //call getDate() when loading the page
            getDate();
        </script>
                    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
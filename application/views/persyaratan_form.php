<!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                
                  <h3 class='box-title'>PERSYARATAN</h3>
                      <div class='box box-primary'>
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Id Customer <?php echo form_error('id_customer') ?></td>
            <td><input type="text" class="form-control" name="id_customer" id="id_customer" placeholder="Id Customer" value="<?php echo $id_customer; ?>" />
        </td>
	    <tr><td>Paspor <?php echo form_error('paspor') ?></td>
            <td><textarea class="form-control" rows="3" name="paspor" id="paspor" placeholder="Paspor"><?php echo $paspor; ?></textarea>
        </td></tr>
	    <tr><td>Surat Nikah <?php echo form_error('surat_nikah') ?></td>
            <td><textarea class="form-control" rows="3" name="surat_nikah" id="surat_nikah" placeholder="Surat Nikah"><?php echo $surat_nikah; ?></textarea>
        </td></tr>
	    <tr><td>Ktp <?php echo form_error('ktp') ?></td>
            <td><textarea class="form-control" rows="3" name="ktp" id="ktp" placeholder="Ktp"><?php echo $ktp; ?></textarea>
        </td></tr>
	    <tr><td>Kartu Keluarga <?php echo form_error('kartu_keluarga') ?></td>
            <td><textarea class="form-control" rows="3" name="kartu_keluarga" id="kartu_keluarga" placeholder="Kartu Keluarga"><?php echo $kartu_keluarga; ?></textarea>
        </td></tr>
	    <tr><td>Kartu Kuning <?php echo form_error('kartu_kuning') ?></td>
            <td><textarea class="form-control" rows="3" name="kartu_kuning" id="kartu_kuning" placeholder="Kartu Kuning"><?php echo $kartu_kuning; ?></textarea>
        </td></tr>
	    <input type="hidden" name="id_persyaratan" value="<?php echo $id_persyaratan; ?>" /> 
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('persyaratan') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table></form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
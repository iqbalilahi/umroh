
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                  <h3 class='box-title'>PENDAFTARAN LIST <?php echo anchor('pendaftaran/create/','Create',array('class'=>'btn btn-danger btn-sm'));?>
		<?php //echo anchor(site_url('pendaftaran/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
		<?php //echo anchor(site_url('pendaftaran/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
		<?php 
        //echo anchor(site_url('pendaftaran/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>
                </div><!-- /.box-header -->
                <div class='box-body'>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Nama Customer</th>
		    <th>Nama Paket</th>
		    <th>Tgl Daftar</th>
		    <th>Tgl Bayar</th>
		    <th>Status Pesanan</th>
		    <th>Status Bayar</th>
		    <th>Status Verifikasi Berkas</th>
		    <!-- <th>Action</th> -->
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($pendaftaran_data as $pendaftaran)
            {
                $pesan = $pendaftaran->status_pesanan==1?'Terverifikasi':'Belum Terverifikasi';
                $bayar = $pendaftaran->status_bayar==1?'Sudah Bayar':'Belum Bayar';
                $berkas = $pendaftaran->status_verifikasi_berkas==1?'Sudah Mengirim Berkas':'Belum Mengirim Berkas';
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $pendaftaran->nama_lengkap_c ?></td>
		    <td><?php echo $pendaftaran->nama_paket ; echo "<br> Harga Paket : " ; echo $pendaftaran->harga_paket ?></td>
		    <td><?php echo $pendaftaran->tgl_daftar ?></td>
		    <td><?php echo $pendaftaran->tgl_bayar ?></td>
		    <td><?php echo $pesan ?></td>
		    <td><?php echo $bayar ?></td>
		    <td><?php echo $berkas ?></td>
		    <!-- <td style="text-align:center" width="140px">
			<?php 
			// echo anchor(site_url('pendaftaran/read/'.$pendaftaran->id),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-danger btn-sm')); 
			// echo '  '; 
			// echo anchor(site_url('pendaftaran/update/'.$pendaftaran->id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-danger btn-sm')); 
			// echo '  '; 
			// echo anchor(site_url('pendaftaran/delete/'.$pendaftaran->id),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td> -->
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
                    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
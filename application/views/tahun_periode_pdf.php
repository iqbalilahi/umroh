<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Tahun_periode List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Tahun</th>
		<th>Kloter</th>
		<th>Tgl Berangkat</th>
		
            </tr><?php
            foreach ($tahun_periode_data as $tahun_periode)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $tahun_periode->tahun ?></td>
		      <td><?php echo $tahun_periode->kloter ?></td>
		      <td><?php echo $tahun_periode->tgl_berangkat ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
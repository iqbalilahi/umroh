<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transportasi extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Transportasi_model');
        $this->load->library('form_validation');
        $this->load->library('encrypt');
    }

    public function index()
    {
        $transportasi = $this->Transportasi_model->get_all();

        $data = array(
            'transportasi_data' => $transportasi
        );

        $this->template->load('template','transportasi_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Transportasi_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_transport' => $row->id_transport,
		'nama_transport' => $row->nama_transport,
		'code_transport' => $row->code_transport,
		'desc_transport' => $row->desc_transport,
		'seat_qty' => $row->seat_qty,
	    );
            $this->template->load('template','transportasi_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transportasi'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('transportasi/create_action'),
	    'id_transport' => set_value('id_transport'),
	    'nama_transport' => set_value('nama_transport'),
	    'code_transport' => set_value('code_transport'),
	    'desc_transport' => set_value('desc_transport'),
	    'seat_qty' => set_value('seat_qty'),
	);
        $this->template->load('template','transportasi_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_transport' => $this->input->post('nama_transport',TRUE),
		'code_transport' => $this->input->post('code_transport',TRUE),
		'desc_transport' => $this->input->post('desc_transport',TRUE),
		'seat_qty' => $this->input->post('seat_qty',TRUE),
	    );

            $this->Transportasi_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('transportasi'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Transportasi_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('transportasi/update_action'),
		'id_transport' => set_value('id_transport', $row->id_transport),
		'nama_transport' => set_value('nama_transport', $row->nama_transport),
		'code_transport' => set_value('code_transport', $row->code_transport),
		'desc_transport' => set_value('desc_transport', $row->desc_transport),
		'seat_qty' => set_value('seat_qty', $row->seat_qty),
	    );
            $this->template->load('template','transportasi_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transportasi'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_transport', TRUE));
        } else {
            $data = array(
		'nama_transport' => $this->input->post('nama_transport',TRUE),
		'code_transport' => $this->input->post('code_transport',TRUE),
		'desc_transport' => $this->input->post('desc_transport',TRUE),
		'seat_qty' => $this->input->post('seat_qty',TRUE),
	    );

            $this->Transportasi_model->update($this->input->post('id_transport', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('transportasi'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Transportasi_model->get_by_id($id);

        if ($row) {
            $this->Transportasi_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('transportasi'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transportasi'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_transport', 'nama transport', 'trim|required');
	$this->form_validation->set_rules('code_transport', 'code transport', 'trim|required');
	$this->form_validation->set_rules('desc_transport', 'desc transport', 'trim|required');
	$this->form_validation->set_rules('seat_qty', 'seat qty', 'trim|required');

	$this->form_validation->set_rules('id_transport', 'id_transport', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "transportasi.xls";
        $judul = "transportasi";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Transport");
	xlsWriteLabel($tablehead, $kolomhead++, "Code Transport");
	xlsWriteLabel($tablehead, $kolomhead++, "Desc Transport");
	xlsWriteLabel($tablehead, $kolomhead++, "Seat Qty");

	foreach ($this->Transportasi_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_transport);
	    xlsWriteLabel($tablebody, $kolombody++, $data->code_transport);
	    xlsWriteLabel($tablebody, $kolombody++, $data->desc_transport);
	    xlsWriteNumber($tablebody, $kolombody++, $data->seat_qty);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=transportasi.doc");

        $data = array(
            'transportasi_data' => $this->Transportasi_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('transportasi_doc',$data);
    }

}

/* End of file Transportasi.php */
/* Location: ./application/controllers/Transportasi.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-10-08 05:04:00 */
/* http://harviacode.com */
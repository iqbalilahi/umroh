<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tahun_periode extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tahun_periode_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $tahun_periode = $this->Tahun_periode_model->get_all();

        $data = array(
            'tahun_periode_data' => $tahun_periode
        );

        $this->template->load('template','tahun_periode_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tahun_periode_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_tahun' => $row->id_tahun,
		'tahun' => $row->tahun,
		'kloter' => $row->kloter,
		'tgl_berangkat' => $row->tgl_berangkat,
	    );
            $this->template->load('template','tahun_periode_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tahun_periode'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tahun_periode/create_action'),
	    'id_tahun' => set_value('id_tahun'),
	    'tahun' => set_value('tahun'),
	    'kloter' => set_value('kloter'),
	    'tgl_berangkat' => set_value('tgl_berangkat'),
	);
        $this->template->load('template','tahun_periode_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'tahun' => $this->input->post('tahun',TRUE),
		'kloter' => $this->input->post('kloter',TRUE),
		'tgl_berangkat' => $this->input->post('tgl_berangkat',TRUE),
	    );

            $this->Tahun_periode_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tahun_periode'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tahun_periode_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tahun_periode/update_action'),
		'id_tahun' => set_value('id_tahun', $row->id_tahun),
		'tahun' => set_value('tahun', $row->tahun),
		'kloter' => set_value('kloter', $row->kloter),
		'tgl_berangkat' => set_value('tgl_berangkat', $row->tgl_berangkat),
	    );
            $this->template->load('template','tahun_periode_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tahun_periode'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_tahun', TRUE));
        } else {
            $data = array(
		'tahun' => $this->input->post('tahun',TRUE),
		'kloter' => $this->input->post('kloter',TRUE),
		'tgl_berangkat' => $this->input->post('tgl_berangkat',TRUE),
	    );

            $this->Tahun_periode_model->update($this->input->post('id_tahun', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tahun_periode'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tahun_periode_model->get_by_id($id);

        if ($row) {
            $this->Tahun_periode_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tahun_periode'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tahun_periode'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('tahun', 'tahun', 'trim|required');
	$this->form_validation->set_rules('kloter', 'kloter', 'trim|required');
	$this->form_validation->set_rules('tgl_berangkat', 'tgl berangkat', 'trim|required');

	$this->form_validation->set_rules('id_tahun', 'id_tahun', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tahun_periode.xls";
        $judul = "tahun_periode";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Tahun");
	xlsWriteLabel($tablehead, $kolomhead++, "Kloter");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Berangkat");

	foreach ($this->Tahun_periode_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->tahun);
	    xlsWriteNumber($tablebody, $kolombody++, $data->kloter);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_berangkat);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=tahun_periode.doc");

        $data = array(
            'tahun_periode_data' => $this->Tahun_periode_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('tahun_periode_doc',$data);
    }

}

/* End of file Tahun_periode.php */
/* Location: ./application/controllers/Tahun_periode.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-10-29 14:25:06 */
/* http://harviacode.com */
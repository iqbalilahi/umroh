<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotel extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kamar_model');
        $this->load->model('Hotel_model');
        $this->load->library('form_validation');
        $this->load->library('encrypt');
    }

    public function index()
    {
        $hotel = $this->Hotel_model->join_kamar();
        $data = array(
            'hotel_data' => $hotel
        );

        $this->template->load('template','hotel_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Hotel_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_hotel' => $row->id_hotel,
		'id_kamar' => $row->id_kamar,
        // 'nama_kamar' => $rows->nama_kamar,
        // 'banyak_kasur' => $rows->banyak_kasur,
		'nama_hotel' => $row->nama_hotel,
		'lokasi_hotel' => $row->lokasi_hotel,
		'desc_hotel' => $row->desc_hotel,
	    );
            $this->template->load('template','hotel_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('hotel'));
        }
    }

    public function create() 
    {
    $data = array(
        'button' => 'Create',
        'action' => site_url('hotel/create_action'),
	    'id_hotel' => set_value('id_hotel'),
        'id_kamar' => $this->Kamar_model->get_all(),
        'kamar_selected' => '',
	    'nama_hotel' => set_value('nama_hotel'),
	    'lokasi_hotel' => set_value('lokasi_hotel'),
	    'desc_hotel' => set_value('desc_hotel'),
    );
        $this->template->load('template','hotel_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_kamar' => $this->input->post('id_kamar',TRUE),
		'nama_hotel' => $this->input->post('nama_hotel',TRUE),
		'lokasi_hotel' => $this->input->post('lokasi_hotel',TRUE),
		'desc_hotel' => $this->input->post('desc_hotel',TRUE),
	    );

            $this->Hotel_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('hotel'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Hotel_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('hotel/update_action'),
		'id_hotel' => set_value('id_hotel', $row->id_hotel),
        'id_kamar' => $this->Kamar_model->get_all(),
        'kamar_selected' => $row->id_kamar,
		'nama_hotel' => set_value('nama_hotel', $row->nama_hotel),
		'lokasi_hotel' => set_value('lokasi_hotel', $row->lokasi_hotel),
		'desc_hotel' => set_value('desc_hotel', $row->desc_hotel),
	    );
            $this->template->load('template','hotel_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('hotel'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_hotel', TRUE));
        } else {
            $data = array(
		'id_kamar' => $this->input->post('id_kamar',TRUE),
		'nama_hotel' => $this->input->post('nama_hotel',TRUE),
		'lokasi_hotel' => $this->input->post('lokasi_hotel',TRUE),
		'desc_hotel' => $this->input->post('desc_hotel',TRUE),
	    );

            $this->Hotel_model->update($this->input->post('id_hotel', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('hotel'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Hotel_model->get_by_id($id);

        if ($row) {
            $this->Hotel_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('hotel'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('hotel'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_kamar', 'id kamar', 'trim|required');
	$this->form_validation->set_rules('nama_hotel', 'nama hotel', 'trim|required');
	$this->form_validation->set_rules('lokasi_hotel', 'lokasi hotel', 'trim|required');
	$this->form_validation->set_rules('desc_hotel', 'desc hotel', 'trim|required');

	$this->form_validation->set_rules('id_hotel', 'id_hotel', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "hotel.xls";
        $judul = "hotel";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Kamar");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Hotel");
	xlsWriteLabel($tablehead, $kolomhead++, "Lokasi Hotel");
	xlsWriteLabel($tablehead, $kolomhead++, "Desc Hotel");

	foreach ($this->Hotel_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_kamar);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_hotel);
	    xlsWriteLabel($tablebody, $kolombody++, $data->lokasi_hotel);
	    xlsWriteLabel($tablebody, $kolombody++, $data->desc_hotel);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=hotel.doc");

        $data = array(
            'hotel_data' => $this->Hotel_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('hotel_doc',$data);
    }

}

/* End of file Hotel.php */
/* Location: ./application/controllers/Hotel.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-10-08 05:06:16 */
/* http://harviacode.com */
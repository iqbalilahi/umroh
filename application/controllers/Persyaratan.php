<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Persyaratan extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Persyaratan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $persyaratan = $this->Persyaratan_model->join_cust();

        $data = array(
            'persyaratan_data' => $persyaratan
        );

        $this->template->load('template','persyaratan_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Persyaratan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_persyaratan' => $row->id_persyaratan,
		'id_customer' => $row->id_customer,
		'paspor' => $row->paspor,
		'surat_nikah' => $row->surat_nikah,
		'ktp' => $row->ktp,
		'kartu_keluarga' => $row->kartu_keluarga,
		'kartu_kuning' => $row->kartu_kuning,
	    );
            $this->template->load('template','persyaratan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('persyaratan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('persyaratan/create_action'),
	    'id_persyaratan' => set_value('id_persyaratan'),
	    'id_customer' => set_value('id_customer'),
	    'paspor' => set_value('paspor'),
	    'surat_nikah' => set_value('surat_nikah'),
	    'ktp' => set_value('ktp'),
	    'kartu_keluarga' => set_value('kartu_keluarga'),
	    'kartu_kuning' => set_value('kartu_kuning'),
	);
        $this->template->load('template','persyaratan_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_customer' => $this->input->post('id_customer',TRUE),
		'paspor' => $this->input->post('paspor',TRUE),
		'surat_nikah' => $this->input->post('surat_nikah',TRUE),
		'ktp' => $this->input->post('ktp',TRUE),
		'kartu_keluarga' => $this->input->post('kartu_keluarga',TRUE),
		'kartu_kuning' => $this->input->post('kartu_kuning',TRUE),
	    );

            $this->Persyaratan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('persyaratan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Persyaratan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('persyaratan/update_action'),
		'id_persyaratan' => set_value('id_persyaratan', $row->id_persyaratan),
		'id_customer' => set_value('id_customer', $row->id_customer),
		'paspor' => set_value('paspor', $row->paspor),
		'surat_nikah' => set_value('surat_nikah', $row->surat_nikah),
		'ktp' => set_value('ktp', $row->ktp),
		'kartu_keluarga' => set_value('kartu_keluarga', $row->kartu_keluarga),
		'kartu_kuning' => set_value('kartu_kuning', $row->kartu_kuning),
	    );
            $this->template->load('template','persyaratan_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('persyaratan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_persyaratan', TRUE));
        } else {
            $data = array(
		'id_customer' => $this->input->post('id_customer',TRUE),
		'paspor' => $this->input->post('paspor',TRUE),
		'surat_nikah' => $this->input->post('surat_nikah',TRUE),
		'ktp' => $this->input->post('ktp',TRUE),
		'kartu_keluarga' => $this->input->post('kartu_keluarga',TRUE),
		'kartu_kuning' => $this->input->post('kartu_kuning',TRUE),
	    );

            $this->Persyaratan_model->update($this->input->post('id_persyaratan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('persyaratan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Persyaratan_model->get_by_id($id);

        if ($row) {
            $this->Persyaratan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('persyaratan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('persyaratan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_customer', 'id customer', 'trim|required');
	$this->form_validation->set_rules('paspor', 'paspor', 'trim|required');
	$this->form_validation->set_rules('surat_nikah', 'surat nikah', 'trim|required');
	$this->form_validation->set_rules('ktp', 'ktp', 'trim|required');
	$this->form_validation->set_rules('kartu_keluarga', 'kartu keluarga', 'trim|required');
	$this->form_validation->set_rules('kartu_kuning', 'kartu kuning', 'trim|required');

	$this->form_validation->set_rules('id_persyaratan', 'id_persyaratan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Persyaratan.php */
/* Location: ./application/controllers/Persyaratan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-11-08 03:09:41 */
/* http://harviacode.com */
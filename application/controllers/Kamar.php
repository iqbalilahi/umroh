<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kamar extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kamar_model');
        $this->load->library('form_validation');
        $this->load->library('encrypt');
    }

    public function index()
    {
        $kamar = $this->Kamar_model->get_all();

        $data = array(
            'kamar_data' => $kamar
        );

        $this->template->load('template','kamar_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Kamar_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_kamar' => $row->id_kamar,
		'jenis_kamar' => $row->jenis_kamar,
		'nama_kamar' => $row->nama_kamar,
		'banyak_kasur' => $row->banyak_kasur,
	    );
            $this->template->load('template','kamar_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kamar'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('kamar/create_action'),
	    'id_kamar' => set_value('id_kamar'),
	    'jenis_kamar' => set_value('jenis_kamar'),
	    'nama_kamar' => set_value('nama_kamar'),
	    'banyak_kasur' => set_value('banyak_kasur'),
	);
        $this->template->load('template','kamar_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'jenis_kamar' => $this->input->post('jenis_kamar',TRUE),
		'nama_kamar' => $this->input->post('nama_kamar',TRUE),
		'banyak_kasur' => $this->input->post('banyak_kasur',TRUE),
	    );

            $this->Kamar_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('kamar'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Kamar_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('kamar/update_action'),
		'id_kamar' => set_value('id_kamar', $row->id_kamar),
		'jenis_kamar' => set_value('jenis_kamar', $row->jenis_kamar),
		'nama_kamar' => set_value('nama_kamar', $row->nama_kamar),
		'banyak_kasur' => set_value('banyak_kasur', $row->banyak_kasur),
	    );
            $this->template->load('template','kamar_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kamar'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_kamar', TRUE));
        } else {
            $data = array(
		'jenis_kamar' => $this->input->post('jenis_kamar',TRUE),
		'nama_kamar' => $this->input->post('nama_kamar',TRUE),
		'banyak_kasur' => $this->input->post('banyak_kasur',TRUE),
	    );

            $this->Kamar_model->update($this->input->post('id_kamar', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('kamar'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Kamar_model->get_by_id($id);

        if ($row) {
            $this->Kamar_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('kamar'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kamar'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('jenis_kamar', 'jenis kamar', 'trim|required');
	$this->form_validation->set_rules('nama_kamar', 'nama kamar', 'trim|required');
	$this->form_validation->set_rules('banyak_kasur', 'banyak kasur', 'trim|required');

	$this->form_validation->set_rules('id_kamar', 'id_kamar', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "kamar.xls";
        $judul = "kamar";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Kamar");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Kamar");
	xlsWriteLabel($tablehead, $kolomhead++, "Banyak Kasur");

	foreach ($this->Kamar_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenis_kamar);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_kamar);
	    xlsWriteNumber($tablebody, $kolombody++, $data->banyak_kasur);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=kamar.doc");

        $data = array(
            'kamar_data' => $this->Kamar_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('kamar_doc',$data);
    }

}

/* End of file Kamar.php */
/* Location: ./application/controllers/Kamar.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-10-08 05:07:46 */
/* http://harviacode.com */
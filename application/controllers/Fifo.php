<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fifo extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pendaftaran_model');
        $this->load->library('form_validation');
        
    }

    public function index()
    {
        $pendaftaran = $this->Pendaftaran_model->join_berangkat();
        $data = array(
            'pendaftaran_data' => $pendaftaran
        );

        $this->template->load('template','fifo_list', $data);
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_customer', 'id customer', 'trim|required');
	$this->form_validation->set_rules('id_paket', 'id paket', 'trim|required');
	$this->form_validation->set_rules('tgl_daftar', 'tgl daftar', 'trim|required');
	$this->form_validation->set_rules('tgl_berangkat', 'tgl berangkat', 'trim|required');
	$this->form_validation->set_rules('status_pesanan', 'status pesanan', 'trim|required');
	$this->form_validation->set_rules('status_bayar', 'status bayar', 'trim|required');
	$this->form_validation->set_rules('status_verifikasi_berkas', 'status verifikasi berkas', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "pendaftaran.xls";
        $judul = "pendaftaran";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Customer");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Paket");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Daftar");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Berangkat");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Pesanan");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Bayar");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Verifikasi Berkas");

	foreach ($this->Pendaftaran_model->join_berangkat() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_lengkap_c);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_paket);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_daftar);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_berangkat);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status_pesanan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status_bayar);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status_verifikasi_berkas);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=pendaftaran.doc");

        $data = array(
            'pendaftaran_data' => $this->Pendaftaran_model->join_berangkat(),
            'start' => 0
        );
        
        $this->load->view('pendaftaran_doc',$data);
    }
     public function fifo_pdf()
    {
        $this->load->library('Pdf');
            $pendaftaran = $this->Pendaftaran_model->join_berangkat();
            $data = array(
            'pendaftaran_data' => $pendaftaran
        );
            $this->template->load('template','fifo_pdf', $data);
    }   

    public function berangkat_pdf($id)
    {
        $this->load->library('Pdf');
        $row = $this->Pendaftaran_model->hasil_transaksi($id);
       //  print_r($row); exit();
        if ($row) {
            $data = array(
        'id' => $row->id,
        'nama_lengkap_c' => $row->nama_lengkap_c,
        'nama_paket' => $row->nama_paket,
        'harga_paket' => $row->harga_paket,
        'tgl_berangkat' => $row->tgl_berangkat,
        'tgl_daftar' => $row->tgl_daftar,
        'tgl_bayar' => $row->tgl_bayar,
        'status_pesanan' => $row->status_pesanan,
        'status_bayar' => $row->status_bayar,
        'status_verifikasi_berkas' => $row->status_verifikasi_berkas,
        );
            $this->template->load('template','berangkat_pdf', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('fifo'));
        }
    } 

}

/* End of file Pendaftaran.php */
/* Location: ./application/controllers/Pendaftaran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-10-23 05:22:57 */
/* http://harviacode.com */
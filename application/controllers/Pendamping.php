<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pendamping extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pendamping_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $pendamping = $this->Pendamping_model->get_all();

        $data = array(
            'pendamping_data' => $pendamping
        );

        $this->template->load('template','pendamping_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Pendamping_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_pendamping' => $row->id_pendamping,
		'nama_lengkap_p' => $row->nama_lengkap_p,
		'tempat' => $row->tempat,
		'tanggal_ahir' => $row->tanggal_ahir,
		'jenis_kelamin' => $row->jenis_kelamin,
		'no_telp' => $row->no_telp,
		'alamat' => $row->alamat,
	    );
            $this->template->load('template','pendamping_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pendamping'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pendamping/create_action'),
	    'id_pendamping' => set_value('id_pendamping'),
	    'nama_lengkap_p' => set_value('nama_lengkap_p'),
	    'tempat' => set_value('tempat'),
	    'tanggal_ahir' => set_value('tanggal_ahir'),
	    'jenis_kelamin' => set_value('jenis_kelamin'),
	    'no_telp' => set_value('no_telp'),
	    'alamat' => set_value('alamat'),
	);
        $this->template->load('template','pendamping_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_lengkap_p' => $this->input->post('nama_lengkap_p',TRUE),
		'tempat' => $this->input->post('tempat',TRUE),
		'tanggal_ahir' => $this->input->post('tanggal_ahir',TRUE),
		'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Pendamping_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pendamping'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pendamping_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pendamping/update_action'),
		'id_pendamping' => set_value('id_pendamping', $row->id_pendamping),
		'nama_lengkap_p' => set_value('nama_lengkap_p', $row->nama_lengkap_p),
		'tempat' => set_value('tempat', $row->tempat),
		'tanggal_ahir' => set_value('tanggal_ahir', $row->tanggal_ahir),
		'jenis_kelamin' => set_value('jenis_kelamin', $row->jenis_kelamin),
		'no_telp' => set_value('no_telp', $row->no_telp),
		'alamat' => set_value('alamat', $row->alamat),
	    );
            $this->template->load('template','pendamping_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pendamping'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pendamping', TRUE));
        } else {
            $data = array(
		'nama_lengkap_p' => $this->input->post('nama_lengkap_p',TRUE),
		'tempat' => $this->input->post('tempat',TRUE),
		'tanggal_ahir' => $this->input->post('tanggal_ahir',TRUE),
		'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Pendamping_model->update($this->input->post('id_pendamping', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pendamping'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pendamping_model->get_by_id($id);

        if ($row) {
            $this->Pendamping_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pendamping'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pendamping'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_lengkap_p', 'nama lengkap p', 'trim|required');
	$this->form_validation->set_rules('tempat', 'tempat', 'trim|required');
	$this->form_validation->set_rules('tanggal_ahir', 'tanggal ahir', 'trim|required');
	$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
	$this->form_validation->set_rules('no_telp', 'no telp', 'trim|required|numeric|min_length[10]|max_length[13]');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');

	$this->form_validation->set_rules('id_pendamping', 'id_pendamping', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "pendamping.xls";
        $judul = "pendamping";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Lengkap");
	xlsWriteLabel($tablehead, $kolomhead++, "Tempat");
	xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Ahir");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Kelamin");
	xlsWriteLabel($tablehead, $kolomhead++, "No Telp");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat");

	foreach ($this->Pendamping_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_lengkap_p);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tempat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tanggal_ahir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenis_kelamin);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_telp);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=pendamping.doc");

        $data = array(
            'pendamping_data' => $this->Pendamping_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('pendamping_doc',$data);
    }

}

/* End of file Pendamping.php */
/* Location: ./application/controllers/Pendamping.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-10-08 09:28:01 */
/* http://harviacode.com */
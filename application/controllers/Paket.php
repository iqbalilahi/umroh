<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Paket extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Paket_model');
        $this->load->model('Hotel_model');
        $this->load->model('Transportasi_model');
        $this->load->model('Pendamping_model');
        $this->load->model('Tahun_periode_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $paket = $this->Paket_model->join_paket();

        $data = array(
            'paket_data' => $paket
        );

        $this->template->load('template','paket_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Paket_model->transaksi_paket($id);
        
        if ($row) {
            $data = array(
		'id_paket' => $row->id_paket,
		'nama_paket' => $row->nama_paket,
		'id_hotel' => $row->id_hotel,
		'id_transport' => $row->id_transport,
		'id_pendamping' => $row->id_pendamping,
        'nama_hotel' => $row->nama_hotel,
        'nama_transport' => $row->nama_transport,
        'nama_lengkap_p' => $row->nama_lengkap_p,
		'id_tahun' => $row->id_tahun,
        'kloter' => $row->kloter,
        'tahun' => $row->tahun,
        'tgl_berangkat' => $row->tgl_berangkat,
		'harga_paket' => $row->harga_paket,
		'desc_paket' => $row->desc_paket,
	    );
            $this->template->load('template','paket_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('paket'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('paket/create_action'),
	    'id_paket' => set_value('id_paket'),
	    'nama_paket' => set_value('nama_paket'),
        'id_hotel' => $this->Hotel_model->get_all(),
        'hotel_selected' => '',
	    'id_transport' => $this->Transportasi_model->get_all(),
        'transport_selected' => '',
	    'id_pendamping' => $this->Pendamping_model->get_all(),
        'pendamping_selected' => '',
	    'id_tahun' => $this->Tahun_periode_model->get_all(),
        'tahun_selected' => '',
	    'harga_paket' => set_value('harga_paket'),
	    'desc_paket' => set_value('desc_paket'),
	);
        $this->template->load('template','paket_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_paket' => $this->input->post('nama_paket',TRUE),
		'id_hotel' => $this->input->post('id_hotel',TRUE),
		'id_transport' => $this->input->post('id_transport',TRUE),
		'id_pendamping' => $this->input->post('id_pendamping',TRUE),
		'id_tahun' => $this->input->post('id_tahun',TRUE),
		'harga_paket' => $this->input->post('harga_paket',TRUE),
		'desc_paket' => $this->input->post('desc_paket',TRUE),
	    );

            $this->Paket_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('paket'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Paket_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('paket/update_action'),
		'id_paket' => set_value('id_paket', $row->id_paket),
		'nama_paket' => set_value('nama_paket', $row->nama_paket),
		'id_hotel' => $this->Hotel_model->get_all(),
        'hotel_selected' => $row->id_hotel,
        'id_transport' => $this->Transportasi_model->get_all(),
        'transport_selected' => $row->id_transport,
        'id_pendamping' => $this->Pendamping_model->get_all(),
        'pendamping_selected' => $row->id_pendamping,
        'id_tahun' => $this->Tahun_periode_model->get_all(),
        'tahun_selected' => $row->id_tahun,
		'harga_paket' => set_value('harga_paket', $row->harga_paket),
		'desc_paket' => set_value('desc_paket', $row->desc_paket),
	    );
            $this->template->load('template','paket_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('paket'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_paket', TRUE));
        } else {
            $data = array(
		'nama_paket' => $this->input->post('nama_paket',TRUE),
		'id_hotel' => $this->input->post('id_hotel',TRUE),
		'id_transport' => $this->input->post('id_transport',TRUE),
		'id_pendamping' => $this->input->post('id_pendamping',TRUE),
		'id_tahun' => $this->input->post('id_tahun',TRUE),
		'harga_paket' => $this->input->post('harga_paket',TRUE),
		'desc_paket' => $this->input->post('desc_paket',TRUE),
	    );

            $this->Paket_model->update($this->input->post('id_paket', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('paket'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Paket_model->get_by_id($id);

        if ($row) {
            $this->Paket_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('paket'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('paket'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_paket', 'nama paket', 'trim|required');
	$this->form_validation->set_rules('id_hotel', 'id hotel', 'trim|required');
	$this->form_validation->set_rules('id_transport', 'id transport', 'trim|required');
	$this->form_validation->set_rules('id_pendamping', 'id pendamping', 'trim|required');
	$this->form_validation->set_rules('id_tahun', 'id tahun', 'trim|required');
	$this->form_validation->set_rules('harga_paket', 'harga paket', 'trim|required');
	$this->form_validation->set_rules('desc_paket', 'desc paket', 'trim|required');

	$this->form_validation->set_rules('id_paket', 'id_paket', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "paket.xls";
        $judul = "paket";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Paket");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Hotel");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Transport");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Pendamping");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Tahun");
	xlsWriteLabel($tablehead, $kolomhead++, "Harga Paket");
	xlsWriteLabel($tablehead, $kolomhead++, "Desc Paket");

	foreach ($this->Paket_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_paket);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_hotel);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_transport);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_pendamping);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_tahun);
	    xlsWriteLabel($tablebody, $kolombody++, $data->harga_paket);
	    xlsWriteLabel($tablebody, $kolombody++, $data->desc_paket);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=paket.doc");

        $data = array(
            'paket_data' => $this->Paket_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('paket_doc',$data);
    }

}

/* End of file Paket.php */
/* Location: ./application/controllers/Paket.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-10-11 09:12:20 */
/* http://harviacode.com */
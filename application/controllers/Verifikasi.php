<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Verifikasi extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pendaftaran_model');
        $this->load->model('Paket_model');
        $this->load->model('Customer_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $pendaftaran = $this->Pendaftaran_model->join_daftar();
        $data = array(
            'pendaftaran_data' => $pendaftaran
        );

        $this->template->load('template','verifikasi_list', $data);
    }

    public function bayaran($id) 
    {

        $bayaran = $this->input->post('status_bayar');
       // $dah_bayar = $this->input->post('tgl_bayar'); $dah_bayar 
         $dah_bayar = date('Y-m-d');

        $this->Pendaftaran_model->bayaran($dah_bayar,$bayaran,$id);
        redirect('fifo');
    }
    public function pesanan($id) 
    {
        $pesanan = $this->input->post('status_pesanan');
        $this->Pendaftaran_model->pesanan($pesanan,$id);
        redirect('verifikasi');

    }
    public function syarat($id)
    {
    if(isset($_POST['submit'])){
        $this->load->library('upload');
                $dataInfo = array();
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
               // print_r($cpt);exit();
            for($i=0; $i<$cpt; $i++)
                {           
                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

                    $this->upload->initialize($this->set_upload_options());
                    $this->upload->do_upload();
                    $dataInfo[] = $this->upload->data();
                }

                $data = array('id_customer'=> $this->input->post('id_customer'),
                'paspor'=> $dataInfo[0]['file_name'],
                'surat_nikah'=> $dataInfo[1]['file_name'],
                'ktp'=> $dataInfo[2]['file_name'],
                'kartu_keluarga'=> $dataInfo[3]['file_name'],
                'kartu_kuning'=> $dataInfo[4]['file_name'],
            );
                $this->session->set_flashdata('success', 'Data Berhasil disimpan');
                $this->Pendaftaran_model->save_syarat($data);
                $syarat = $this->input->post('status_verifikasi_berkas');
                $this->Pendaftaran_model->syarat($syarat,$id); 
                redirect('verifikasi');
                } else {
                 $this->session->set_flashdata('failed', 'Data Gagal disimpan');
                $this->index();
            }
    }
     private function set_upload_options()
{   
    //upload an image options
    $config = array();
    $config['upload_path'] = './upload/kelengkapan/';
    $config['allowed_types'] = 'gif|jpg|png|pdf';
    $config['max_size']      = '0';
   $config['overwrite']     = FALSE;

    return $config;
}

    public function _rules() 
    {
	$this->form_validation->set_rules('id_customer', 'id customer', 'trim|required');
	$this->form_validation->set_rules('id_paket', 'id paket', 'trim|required');
	$this->form_validation->set_rules('tgl_daftar', 'tgl daftar', 'trim|required');
	$this->form_validation->set_rules('tgl_bayar', 'tgl bayar', 'trim|required');
	$this->form_validation->set_rules('status_pesanan', 'status pesanan', 'trim|required');
	$this->form_validation->set_rules('status_bayar', 'status bayar', 'trim|required');
	$this->form_validation->set_rules('status_verifikasi_berkas', 'status verifikasi berkas', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "pendaftaran.xls";
        $judul = "pendaftaran";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Customer");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Paket");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Daftar");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl bayar");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Pesanan");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Bayar");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Verifikasi Berkas");

	foreach ($this->Pendaftaran_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_customer);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_paket);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_daftar);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_bayar);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status_pesanan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status_bayar);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status_verifikasi_berkas);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=pendaftaran.doc");

        $data = array(
            'pendaftaran_data' => $this->Pendaftaran_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('pendaftaran_doc',$data);
    }

}

/* End of file Pendaftaran.php */
/* Location: ./application/controllers/Pendaftaran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-10-23 05:22:57 */
/* http://harviacode.com */
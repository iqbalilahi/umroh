<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pendaftaran extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pendaftaran_model');
        $this->load->model('Customer_model');
        $this->load->model('Paket_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $pendaftaran = $this->Pendaftaran_model->join_daftar();
        $data = array(
            'pendaftaran_data' => $pendaftaran
        );

        $this->template->load('template','pendaftaran_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Pendaftaran_model->hasil_transaksi($id);
       //  print_r($row); exit();
        if ($row) {
            $data = array(
		'id' => $row->id,
		'nama_lengkap_c' => $row->nama_lengkap_c,
		'nama_paket' => $row->nama_paket,
        'harga_paket' => $row->harga_paket,
        'tgl_berangkat' => $row->tgl_berangkat,
		'tgl_daftar' => $row->tgl_daftar,
		'tgl_bayar' => $row->tgl_bayar,
		'status_pesanan' => $row->status_pesanan,
		'status_bayar' => $row->status_bayar,
		'status_verifikasi_berkas' => $row->status_verifikasi_berkas,
	    );
            $this->template->load('template','pendaftaran_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('fifo'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pendaftaran/create_action'),
	    'id' => set_value('id'),
	    'id_customer' => $this->Customer_model->get_all(),
        'customer_selected' => '',
	    'id_paket' => $this->Paket_model->get_all(),
        'paket_selected' => '',
	    'tgl_daftar' => set_value('tgl_daftar'),
	    'tgl_bayar' => set_value('tgl_bayar'),
	    'status_pesanan' => set_value('status_pesanan'),
	    'status_bayar' => set_value('status_bayar'),
	    'status_verifikasi_berkas' => set_value('status_verifikasi_berkas'),
	);
        $this->template->load('template','pendaftaran_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_customer' => $this->input->post('id_customer',TRUE),
		'id_paket' => $this->input->post('id_paket',TRUE),
		'tgl_daftar' => $this->input->post('tgl_daftar',TRUE),
		'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
		'status_pesanan' => $this->input->post('status_pesanan',TRUE),
		'status_bayar' => $this->input->post('status_bayar',TRUE),
		'status_verifikasi_berkas' => $this->input->post('status_verifikasi_berkas',TRUE),
	    );

            $this->Pendaftaran_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pendaftaran'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pendaftaran_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pendaftaran/update_action'),
		'id' => set_value('id', $row->id),
		'id_customer' => $this->Customer_model->get_all(),
        'customer_selected' => $row->id_customer,
		'id_paket' => $this->Paket_model->get_all(),
        'paket_selected' => $row->id_paket,
		'tgl_daftar' => set_value('tgl_daftar', $row->tgl_daftar),
		'tgl_bayar' => set_value('tgl_bayar', $row->tgl_bayar),
		'status_pesanan' => set_value('status_pesanan', $row->status_pesanan),
		'status_bayar' => set_value('status_bayar', $row->status_bayar),
		'status_verifikasi_berkas' => set_value('status_verifikasi_berkas', $row->status_verifikasi_berkas),
	    );
            $this->template->load('template','pendaftaran_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pendaftaran'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'id_customer' => $this->input->post('id_customer',TRUE),
		'id_paket' => $this->input->post('id_paket',TRUE),
		'tgl_daftar' => $this->input->post('tgl_daftar',TRUE),
		'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
		'status_pesanan' => $this->input->post('status_pesanan',TRUE),
		'status_bayar' => $this->input->post('status_bayar',TRUE),
		'status_verifikasi_berkas' => $this->input->post('status_verifikasi_berkas',TRUE),
	    );

            $this->Pendaftaran_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pendaftaran'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pendaftaran_model->get_by_id($id);

        if ($row) {
            $this->Pendaftaran_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pendaftaran'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pendaftaran'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_customer', 'id customer', 'trim|required');
	$this->form_validation->set_rules('id_paket', 'id paket', 'trim|required');
	$this->form_validation->set_rules('tgl_daftar', 'tgl daftar', 'trim|required');
	$this->form_validation->set_rules('tgl_bayar', 'tgl bayar', 'trim|required');
	$this->form_validation->set_rules('status_pesanan', 'status pesanan', 'trim|required');
	$this->form_validation->set_rules('status_bayar', 'status bayar', 'trim|required');
	$this->form_validation->set_rules('status_verifikasi_berkas', 'status verifikasi berkas', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "pendaftaran.xls";
        $judul = "pendaftaran";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Customer");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Paket");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Daftar");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Bayar");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Pesanan");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Bayar");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Verifikasi Berkas");

	foreach ($this->Pendaftaran_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_customer);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_paket);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_daftar);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_bayar);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status_pesanan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status_bayar);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status_verifikasi_berkas);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=pendaftaran.doc");

        $data = array(
            'pendaftaran_data' => $this->Pendaftaran_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('pendaftaran_doc',$data);
    }

}

/* End of file Pendaftaran.php */
/* Location: ./application/controllers/Pendaftaran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-10-23 05:22:57 */
/* http://harviacode.com */
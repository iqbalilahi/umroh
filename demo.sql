-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2019 at 08:05 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` bigint(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `nama_lengkap_c` varchar(100) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `penyakit_khusus` varchar(50) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `username`, `nama_lengkap_c`, `nama_ibu`, `tempat`, `tanggal_lahir`, `jenis_kelamin`, `penyakit_khusus`, `no_telp`, `alamat`, `password`) VALUES
(1, 'iqbal', 'iqbalilahi', 'ibu', 'Bekasi', '1997-06-04', 'Laki-laki', 'gada', '087881456735', 'Aren Jaya', '5ebe2294ecd0e0f08eab7690d2a6ee69'),
(2, 'reza', 'rezaahbati', 'markonah', 'Bekasi', '1996-10-27', 'Laki-laki', 'gada', '087881456733', 'Cikunir', '5f4dcc3b5aa765d61d8327deb882cf99');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id_hotel` bigint(20) NOT NULL,
  `id_kamar` bigint(20) NOT NULL,
  `nama_hotel` varchar(100) NOT NULL,
  `lokasi_hotel` varchar(150) NOT NULL,
  `desc_hotel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id_hotel`, `id_kamar`, `nama_hotel`, `lokasi_hotel`, `desc_hotel`) VALUES
(1, 9, 'Rafles Hotel Mekkah', 'Menara Jam Mekah', 'VVIP Family'),
(2, 8, 'Rafles Hotel Mekkah', 'Menara Jam Mekah', 'VVIP Standart'),
(3, 7, 'Rafles Hotel Mekkah', 'Menara Jam Mekah', 'VVIP Standart'),
(4, 6, 'Hilton Mekkah Hotel', 'Strategis', 'VIP Family'),
(5, 5, 'Hilton Mekkah Hotel', 'Strategis', 'VIP Standart'),
(6, 4, 'Hilton Mekkah Hotel', 'Strategis', 'VIP Standart'),
(7, 3, 'Al Waleed Tower Hotel', 'Prince Sultan road', 'Reguler Family'),
(8, 2, 'Al Waleed Tower Hotel', 'Prince Sultan road', 'Reguler Standart'),
(9, 1, 'Al Waleed Tower Hotel', 'Prince Sultan road', 'Reguler Standart');

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE `kamar` (
  `id_kamar` bigint(20) NOT NULL,
  `jenis_kamar` varchar(50) NOT NULL,
  `nama_kamar` varchar(100) NOT NULL,
  `banyak_kasur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`id_kamar`, `jenis_kamar`, `nama_kamar`, `banyak_kasur`) VALUES
(1, 'Standar Twin', 'Twin Room', 2),
(2, 'Standar Double', 'Double room', 2),
(3, 'Standar Family', 'Family room', 4),
(4, 'Deluxe Double', 'Double room', 2),
(5, 'Deluxe Twin', 'Twin Room', 2),
(6, 'Deluxe Family', 'Family room', 4),
(7, 'Presidental Twin', 'Twin Room', 2),
(8, 'Presidental Double', 'Double room', 2),
(9, 'Presidental Family', 'Family room', 4);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  `is_parent` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `link`, `icon`, `is_active`, `is_parent`) VALUES
(1, 'menu management', 'menu', 'fa fa-list-alt', 1, 0),
(2, 'Customer', 'customer', 'fa fa-users', 1, 24),
(18, 'Pendamping', 'pendamping', 'fa fa-street-view', 1, 24),
(19, 'Transportasi', 'transportasi', 'fa fa-plane', 1, 24),
(20, 'Hotel', 'hotel', 'fa  fa-building', 1, 24),
(21, 'Kamar', 'kamar', 'fa fa-bed', 1, 24),
(22, 'Tahun Periode', 'tahun_periode', 'fa fa-calendar-o', 1, 24),
(23, 'Paket', 'paket', 'fa fa-sticky-note', 1, 24),
(24, 'Master', '#', 'fa fa-save', 1, 0),
(25, 'Pendaftaran', 'pendaftaran', 'fa fa-newspaper-o', 1, 0),
(26, 'Verifikasi', 'verifikasi', 'fa fa-check-square', 1, 0),
(27, 'FIFO', 'fifo', 'fa fa-television', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `id_paket` bigint(8) NOT NULL,
  `nama_paket` varchar(100) NOT NULL,
  `id_hotel` bigint(8) NOT NULL,
  `id_transport` bigint(8) NOT NULL,
  `id_pendamping` bigint(8) NOT NULL,
  `id_tahun` bigint(8) NOT NULL,
  `harga_paket` bigint(20) NOT NULL,
  `desc_paket` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id_paket`, `nama_paket`, `id_hotel`, `id_transport`, `id_pendamping`, `id_tahun`, `harga_paket`, `desc_paket`) VALUES
(1, 'VVIP', 1, 2, 1, 2, 28000000, 'Paket Full VVIP');

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id` int(11) NOT NULL,
  `id_customer` bigint(8) NOT NULL,
  `id_paket` bigint(8) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `tgl_bayar` date NOT NULL,
  `status_pesanan` int(11) NOT NULL,
  `status_bayar` int(11) NOT NULL,
  `status_verifikasi_berkas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendaftaran`
--

INSERT INTO `pendaftaran` (`id`, `id_customer`, `id_paket`, `tgl_daftar`, `tgl_bayar`, `status_pesanan`, `status_bayar`, `status_verifikasi_berkas`) VALUES
(1, 1, 1, '2019-10-23', '0000-00-00', 0, 0, 0),
(2, 2, 1, '2019-10-27', '2019-10-31', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pendamping`
--

CREATE TABLE `pendamping` (
  `id_pendamping` bigint(20) NOT NULL,
  `nama_lengkap_p` varchar(100) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `tanggal_ahir` date NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `alamat` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendamping`
--

INSERT INTO `pendamping` (`id_pendamping`, `nama_lengkap_p`, `tempat`, `tanggal_ahir`, `jenis_kelamin`, `no_telp`, `alamat`) VALUES
(1, 'aki aki', 'Bekasi', '1997-01-01', 'Laki-laki', '087881456735', 'Babelan');

-- --------------------------------------------------------

--
-- Table structure for table `persyaratan`
--

CREATE TABLE `persyaratan` (
  `id_persyaratan` bigint(8) NOT NULL,
  `id_customer` bigint(8) NOT NULL,
  `paspor` varchar(255) NOT NULL,
  `surat_nikah` varchar(255) NOT NULL,
  `ktp` varchar(255) NOT NULL,
  `kartu_keluarga` varchar(255) NOT NULL,
  `kartu_kuning` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tahun_periode`
--

CREATE TABLE `tahun_periode` (
  `id_tahun` bigint(20) NOT NULL,
  `tahun` int(11) NOT NULL,
  `kloter` int(11) NOT NULL,
  `tgl_berangkat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_periode`
--

INSERT INTO `tahun_periode` (`id_tahun`, `tahun`, `kloter`, `tgl_berangkat`) VALUES
(1, 2019, 1, '2019-11-30'),
(2, 2019, 2, '2019-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `transportasi`
--

CREATE TABLE `transportasi` (
  `id_transport` bigint(20) NOT NULL,
  `nama_transport` varchar(100) NOT NULL,
  `code_transport` varchar(100) NOT NULL,
  `desc_transport` varchar(200) NOT NULL,
  `seat_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transportasi`
--

INSERT INTO `transportasi` (`id_transport`, `nama_transport`, `code_transport`, `desc_transport`, `seat_qty`) VALUES
(1, 'Garuda Indonesia', 'GIA', 'Penerbangan Menengah keatas', 90),
(2, 'Etihad Airways', 'ETD', 'Transpot paket kelas atas', 90);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1572936380, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id_hotel`),
  ADD KEY `fk_kamar` (`id_kamar`);

--
-- Indexes for table `kamar`
--
ALTER TABLE `kamar`
  ADD PRIMARY KEY (`id_kamar`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id_paket`),
  ADD KEY `fk_hotelku` (`id_hotel`),
  ADD KEY `fk_transportku` (`id_transport`),
  ADD KEY `fk_pendamping` (`id_pendamping`),
  ADD KEY `fk_tahunku` (`id_tahun`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_custku` (`id_customer`),
  ADD KEY `fk_paketku` (`id_paket`);

--
-- Indexes for table `pendamping`
--
ALTER TABLE `pendamping`
  ADD PRIMARY KEY (`id_pendamping`);

--
-- Indexes for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD PRIMARY KEY (`id_persyaratan`),
  ADD KEY `cust_syarat` (`id_customer`);

--
-- Indexes for table `tahun_periode`
--
ALTER TABLE `tahun_periode`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indexes for table `transportasi`
--
ALTER TABLE `transportasi`
  ADD PRIMARY KEY (`id_transport`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id_hotel` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `kamar`
--
ALTER TABLE `kamar`
  MODIFY `id_kamar` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id_paket` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pendamping`
--
ALTER TABLE `pendamping`
  MODIFY `id_pendamping` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `persyaratan`
--
ALTER TABLE `persyaratan`
  MODIFY `id_persyaratan` bigint(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tahun_periode`
--
ALTER TABLE `tahun_periode`
  MODIFY `id_tahun` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transportasi`
--
ALTER TABLE `transportasi`
  MODIFY `id_transport` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `fk_kamar` FOREIGN KEY (`id_kamar`) REFERENCES `kamar` (`id_kamar`);

--
-- Constraints for table `paket`
--
ALTER TABLE `paket`
  ADD CONSTRAINT `fk_hotelku` FOREIGN KEY (`id_hotel`) REFERENCES `hotel` (`id_hotel`),
  ADD CONSTRAINT `fk_pendamping` FOREIGN KEY (`id_pendamping`) REFERENCES `pendamping` (`id_pendamping`),
  ADD CONSTRAINT `fk_tahunku` FOREIGN KEY (`id_tahun`) REFERENCES `tahun_periode` (`id_tahun`),
  ADD CONSTRAINT `fk_transportku` FOREIGN KEY (`id_transport`) REFERENCES `transportasi` (`id_transport`);

--
-- Constraints for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD CONSTRAINT `fk_custku` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`),
  ADD CONSTRAINT `fk_paketku` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id_paket`);

--
-- Constraints for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD CONSTRAINT `cust_syarat` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
